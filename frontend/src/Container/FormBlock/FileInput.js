import React, { useRef, useState } from "react";
import { makeStyles } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles({
  input: {
    display: "none",
  },
});

const FileInput = ({ onChange, name, label }) => {
  const classes = useStyles();
  const [filename, setFileName] = useState("");

  const inputRef = useRef();

  const activateInput = () => {
    inputRef.current.click();
  };

  const onFileChange = (e) => {
    if (e.target.files[0]) {
      setFileName(e.target.files[0].name);
    } else {
      setFileName("");
    }
    onChange(e);
  };

  return (
    <>
      <input type="file" className={classes.input} ref={inputRef} onChange={onFileChange} name={name} />
      <Grid container spacing={2} alignItems="center">
        <Grid item xs>
          <TextField
            variant="outlined"
            disabled
            fullWidth
            label={label}
            onClick={activateInput}
            value={filename}
          />
        </Grid>
        <Grid item>
          <Button variant="contained" onClick={activateInput}>
            Browse
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default FileInput;
