import { makeStyles } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchMessages } from "../../store/actions";
import Article from "../../Components/Article/Article";

const useStyles = makeStyles((theme) => ({
  chat: {
    marginTop: "50px",
    marginBottom: "50px",
  },
}));

const ChatBlock = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchMessages());
  }, [dispatch]);

  const messages = useSelector((state) => state.messages);

  return (
    <Grid container spacing={3} direction="column" className={classes.chat} wrap="nowrap">
      {messages.map((message) => {
        return (
          <Article key={message.id} author={message.author} image={message.image} message={message.message} />
        );
      })}
    </Grid>
  );
};

export default ChatBlock;
