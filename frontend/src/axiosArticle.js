import axios from "axios";
import { apiURL } from "./config";

const axiosArticle = axios.create({
  baseURL: apiURL,
});

export default axiosArticle;
