import {
  FETCH_MESSAGES_FAILURE,
  FETCH_MESSAGES_REQUEST,
  FETCH_MESSAGES_SUCCESS,
  POST_MESSAGES_FAILURE,
  POST_MESSAGES_REQUEST,
  POST_MESSAGES_SUCCESS,
  SET_MODAL,
} from "./actions";

const initialState = {
  error: false,
  loading: false,
  messages: [],
  modalForm: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_MESSAGES_REQUEST:
      return { ...state, loading: true };
    case FETCH_MESSAGES_SUCCESS:
      return { ...state, loading: false, messages: action.messages };
    case FETCH_MESSAGES_FAILURE:
      return { ...state, loading: false, error: action.error };
    case POST_MESSAGES_REQUEST:
      return { ...state, loading: true };
    case POST_MESSAGES_SUCCESS:
      return { ...state, loading: false };
    case POST_MESSAGES_FAILURE:
      return { ...state, loading: false, error: action.error };
    case SET_MODAL:
      return { ...state, modalForm: action.modalForm };
    default:
      return state;
  }
};

export default reducer;
