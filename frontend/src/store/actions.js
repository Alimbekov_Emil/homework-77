import axiosArticle from "../axiosArticle";

export const FETCH_MESSAGES_REQUEST = "FETCH_MESSAGES_REQUEST";
export const FETCH_MESSAGES_SUCCESS = "FETCH_MESSAGES_SUCCESS";
export const FETCH_MESSAGES_FAILURE = "FETCH_MESSAGES_FAILURE";

export const POST_MESSAGES_REQUEST = "POST_MESSAGES_REQUEST";
export const POST_MESSAGES_SUCCESS = "POST_MESSAGES_SUCCESS";
export const POST_MESSAGES_FAILURE = "POST_MESSAGES_FAILURE";
export const SET_MODAL = "SET_MODAL";

export const fetchMessagesRequest = () => ({ type: FETCH_MESSAGES_REQUEST });
export const fetchMessagesSuccess = (messages) => ({ type: FETCH_MESSAGES_SUCCESS, messages });
export const fetchMessagesFailure = (error) => ({ type: FETCH_MESSAGES_FAILURE, error });

export const postMessagesRequest = () => ({ type: POST_MESSAGES_REQUEST });
export const postMessagesSuccess = () => ({ type: POST_MESSAGES_SUCCESS });
export const postMessagesFailure = (error) => ({ type: POST_MESSAGES_FAILURE, error });

export const setModal = (modalForm) => ({
  type: SET_MODAL,
  modalForm,
});

export const fetchMessages = () => {
  return async (dispatch) => {
    try {
      dispatch(fetchMessagesRequest());
      const response = await axiosArticle.get("/messages");
      dispatch(fetchMessagesSuccess(response.data));
    } catch (e) {
      dispatch(fetchMessagesFailure(e));
    }
  };
};

export const postMessage = (formData) => {
  return async (dispatch) => {
    try {
      dispatch(postMessagesRequest());
      await axiosArticle.post("/messages", formData);
      dispatch(postMessagesSuccess());
      dispatch(fetchMessages());
    } catch (e) {
      dispatch(postMessagesFailure(e));
    }
  };
};
