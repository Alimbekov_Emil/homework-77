import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core";

import { apiURL } from "../../config";

const useStyles = makeStyles({
  card: {
    height: "100%",
  },
  media: {
    height: 0,
    paddingTop: "56.25%",
  },
  mediaNonePhote: {
    display: "none",
  },
});
const Article = ({ author, message, image }) => {
  const classes = useStyles();

  let cardImage = "Image";

  if (image) {
    cardImage = apiURL + "/uploads/" + image;
  }

  return (
    <Grid item xs sm md={12}>
      <Card className={classes.card}>
        <CardHeader title={author} />
        <CardMedia
          image={cardImage}
          title={author}
          className={cardImage !== "Image" ? classes.media : classes.mediaNonePhote}
        />
        <CardContent>
          <strong style={{ marginLeft: "10px" }}>Message: {message} </strong>
        </CardContent>
      </Card>
    </Grid>
  );
};

export default Article;
