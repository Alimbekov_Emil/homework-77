import React from "react";
import Container from "@material-ui/core/Container";
import FormBlock from "./Container/FormBlock/FormBlock";
import ChatBlock from "./Container/ChatBlock/ChatBlock";
import Modal from "./Components/UI/Modal/Modal";
import Brightness7Icon from "@material-ui/icons/Brightness7";
import { Button, LinearProgress, makeStyles } from "@material-ui/core";
import AboutUs from "./Components/AboutUs/AboutUs";
import { useDispatch, useSelector } from "react-redux";
import { setModal } from "./store/actions";

const useStyles = makeStyles({
  header: {
    background: "#1533AD",
    textAlign: "center",
    padding: "5px",
    position: "fixed",
    top: 0,
    left: 0,
    width: "100%",
    zIndex: 1000,
  },
  footer: {
    marginTop: "11.5%",
    background: "#1533AD",
    textAlign: "center",
    padding: "20px",
    color: "white",
  },
  logo: {
    fontSize: "40px",
    color: "white",
    "&:hover": {
      color: "red",
    },
  },
});

const App = () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const loading = useSelector((state) => state.loading);
  const modalForm = useSelector((state) => state.modalForm);

  const modalHandler = () => {
    dispatch(setModal(true));
  };

  const modalCancelHandler = () => {
    dispatch(setModal(false));
  };

  return (
    <>
      <header className={classes.header}>
        <Container maxWidth="md">
          <Button onClick={modalHandler}>
            <Brightness7Icon className={classes.logo} />
          </Button>
        </Container>
        {loading ? <LinearProgress /> : null}
      </header>
      <Container maxWidth="md">
        <AboutUs />
        <ChatBlock />
        <Modal show={modalForm} closed={modalCancelHandler}>
          <FormBlock closed={modalCancelHandler} />
        </Modal>
      </Container>
      <footer className={classes.footer}>
        <Container maxWidth="md"> 2021 Alimbekov224@gmail.com</Container>
      </footer>
    </>
  );
};

export default App;
