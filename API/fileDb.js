const fs = require("fs");
const { nanoid } = require("nanoid");

const filename = "./db.json";

let data = [];

module.exports = {
  init() {
    try {
      const fileContents = fs.readFileSync(filename);
      data = JSON.parse(fileContents);
    } catch (e) {
      data = [];
    }
  },
  getMessage() {
    return data;
  },
  addMessages(message) {
    message.id = nanoid();
    data.push(message);
    this.save();
  },
  save() {
    fs.writeFileSync(filename, JSON.stringify(data, null, 2));
  },
};
