const path = require("path");
const express = require("express");
const fileDb = require("../fileDb");
const multer = require("multer");
const { nanoid } = require("nanoid");
const config = require("../config");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  },
});

const upload = multer({ storage });

const router = express.Router();

router.get("/", (req, res) => {
  const message = fileDb.getMessage();
  res.send(message);
});

router.post("/", upload.single("image"), (req, res) => {
  const message = req.body;
  if (message.message !== "") {
    if (message.author === "") {
      message.author = "Anonymus";
    }

    if (req.file) {
      message.image = req.file.filename;
    }

    fileDb.addMessages(message);
    res.send(message);
  }
});

module.exports = router;
