const express = require("express");
const cors = require("cors");
const app = express();
const messages = require("./app/messages");
const fileDb = require("./fileDb");

app.use(express.json());
app.use(express.static("public"));
app.use(cors());

fileDb.init();
const port = 8001;

app.use("/messages", messages);

app.listen(port, () => {
  console.log(`Server started on ${port} port!`);
});
